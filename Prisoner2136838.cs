namespace PrisonerDilemma;

    class Prisoner2136838 : IPrisoner
    {
        public string Name { get; } = "Alexander";

        private PrisonerChoice otherChoice;
        public void Reset(DilemmaParameters dilemmaParameters) 
        { 

        }

        public PrisonerChoice GetChoice()
        {  
            if (otherChoice == PrisonerChoice.Cooperate)
            {
                return PrisonerChoice.Cooperate;
            }
            return PrisonerChoice.Defect;
        }

        public void ReceiveOtherChoice(PrisonerChoice otherChoice) 
        { 
            this.otherChoice = otherChoice;
        }
    }
