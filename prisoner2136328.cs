namespace PrisonerDilemma
{
    class Prisoner2136328 : IPrisoner {
        public string Name { get; } = "Vijay";

        public PrisonerChoice GetChoice(){
            return PrisonerChoice.Defect;
        }

        public void Reset(DilemmaParameters dilemmaParameters){

        }

        public void ReceiveOtherChoice(PrisonerChoice otherChoice){
        }
    }
}