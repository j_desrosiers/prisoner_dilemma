namespace PrisonerDilemma
{
    class Prisoner2233408 : IPrisoner
    {
        private static Random Random { get; } = new Random();
        private PrisonerChoice choice;

        public string Name { get; } = "Chen";

        public PrisonerChoice GetChoice()
        {
            if (choice == PrisonerChoice.Cooperate){
                return PrisonerChoice.Cooperate;
            }
            return PrisonerChoice.Defect;

            
        }

        public void ReceiveOtherChoice(PrisonerChoice otherChoice)
        {
            choice = otherChoice;
        }

        public void Reset(DilemmaParameters dilemmaParameters)
        {
            
        }
    }
}