using System.Reflection.Metadata.Ecma335;
using PrisonerDilemma;

class Prisoner2043602 : IPrisoner
{
    public string Name {get;} = "2043602";
    private PrisonerChoice? OtherChoice;

    public PrisonerChoice GetChoice()
    {
            // Define random number generator
            Random rng = new Random();
            int choice = rng.Next(4);

            //Cooperate favored, 75% coop chance
            if (OtherChoice == PrisonerChoice.Cooperate) 
            {
                if (choice == 0) {return PrisonerChoice.Defect;}
                else {return PrisonerChoice.Cooperate;}
            }

            //Defect favored, 75% defect chance
            else if (OtherChoice == PrisonerChoice.Defect){
                if (choice == 0) {return PrisonerChoice.Cooperate;}
                else {return PrisonerChoice.Defect;}
            }

            //Blind, 50/50 chance
            else {
                if (choice == 0 || choice == 1) {return PrisonerChoice.Cooperate;}
                else {return PrisonerChoice.Defect;}
            }
    }

    public void Reset(DilemmaParameters dilemmaParameters)
    {
        OtherChoice = null;
    }

    public void ReceiveOtherChoice(PrisonerChoice otherChoice)
    {
        OtherChoice = otherChoice;
    }
}